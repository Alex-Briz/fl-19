// #1
function calculateSum(arr) {
    let sum = 0;
    for (let i in arr) {
        if (parseInt(arr[i], 10)) {
            sum += arr[i];
        }
    }
    return sum;
}

console.log(calculateSum([1, 2, 3, 4, 5])); //15

// #2
function isTriangle(a, b, c) {
    if (a + b > c && b + c > a && c + a > b) {
        return true;
    } else {
        return false;
    }
}

console.log(isTriangle(5, 6, 7)); //true
console.log(isTriangle(2, 9, 3)); //false

// #3
function isIsogram(word) {
    let unique = new Set();
    for (let i in word) {
        unique.add(word[i]);
    }
    if (unique.size === word.length) {
        return true;
    } else {
        return false;
    }
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('abab')); //false

// #4
function isPalindrome(word) {
    const LEN = word.length;
    const MID = Math.floor(LEN / 2);
    let match = 0;
    for (let i = 0; i < MID; i++) {
        if (word[i] === word[LEN - 1 - i]) {
            match++;
        }
    }
    if (match === MID) {
        return true;
    } else {
        return false;
    }
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
    let date = dateObj.getDate();
    const months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    let month = months[dateObj.getMonth()];
    let year = dateObj.getFullYear();
    let resStr = date + ' of ' + month + ', ' + year;
    return resStr;
}

console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => {
    let counter = 0;
    for (let i in str) {
        if (str[i] === letter) {
            counter++;
        }
    }
    return counter;
}

console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
    let resObj = {};
    for (let item in arr) {
        if (!resObj.hasOwnProperty(arr[item])) {
            resObj[arr[item]] = 1;
        } else if (resObj.hasOwnProperty(arr[item])) {
            resObj[arr[item]]++;
        }
    }
    return resObj;
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
    let strBinary = arr.join('');
    let numDecimal = parseInt(strBinary, 2);
    return numDecimal;
}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9