// #1
function extractCurrencyValue(param) {
    let resStr = '';
    for (let i in param) {
        if (parseInt(param[i]) || param[i] === '0') {
            resStr += param[i];
        }
    }
    return resStr;

}

console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) {
    let truthyObj = {};
    for (let key in obj) {
        if (obj[key] !== false && obj[key] !== null && obj[key] !== undefined && obj[key] !== '') {
            truthyObj[key] = obj[key];
        }
    }
    return truthyObj;
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    let to_symbol = Symbol(`\'${param}\'`);
    return to_symbol;
}

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
    let startDateObj = new Date(startDate);
    let endDateObj = new Date(endDate);
    let difference = endDateObj.getTime() - startDateObj.getTime();
    let days = Math.round(difference / 1000 / 60 / 60 / 24);
    let weeks = Math.round(days / 7);
    let months = Math.round(weeks / 4);
    return `The difference between dates is: ${days} day(-s), ${weeks} week(-s), ${months} month(-s)`;
}

console.log(countBetweenTwoDays('03/22/22', '05/25/22')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
    let resArr = [];
    for (let el in arr) {
        if (!resArr.includes(arr[el])) {
            resArr.push(arr[el]);
        }
    }
    return resArr;
}

console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]