function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

if (confirm('Do you want to play a game?')) {
    let attempts = 3;
    let curPrize = 0;
    let maxNum = 3;
    let userInputNum;
    let startPrize = 100;
    let prize = startPrize;
    while (attempts > 0) {
        let randInt = getRandomIntInclusive(0, maxNum);
        userInputNum = parseInt(
            prompt(`Choose a roulette pocket number from 0 to ${maxNum}\n
            Attempts left:${attempts}\nTotal prize:${curPrize}$\n
            Possible prize on current attempt:${prize}$`));
        if (!userInputNum) {
            alert(`Thank you for your participation. Your prize is:${curPrize}$`);
            if (confirm('Do you want to play again?')) {
                attempts = 3;
                startPrize = 100;
                curPrize = 0;
                prize = startPrize;
                randInt = getRandomIntInclusive(0, maxNum);
                continue;
            } else {
                break;
            }

        }
        if (userInputNum === randInt) {
            curPrize += prize;
            if (confirm(`Congratulation, you won! Your prize is: ${curPrize}$. Do you want to continue?`)) {
                maxNum += 2;
                startPrize *= 2;
                prize = startPrize;
                attempts = 3;
                randInt = getRandomIntInclusive(0, maxNum);
                continue;
            } else {
                alert(`Thank you for your participation. Your prize is:${curPrize}$`);
                if (confirm('Do you want to play again?')) {
                    attempts = 3;
                    startPrize = 100;
                    curPrize = 0;
                    maxNum = 3;
                    prize = startPrize;
                    randInt = getRandomIntInclusive(0, maxNum);
                    continue;
                } else {
                    break;
                }
            }
        } else {
            attempts--;
            prize /= 2;
        }
        if (attempts === 0) {
            alert(`Thank you for your participation. Your prize is:${curPrize}$`);
            if (confirm('Do you want to play again?')) {
                curPrize = 0;
                attempts = 3;
                startPrize = 100;
                maxNum = 3;
                prize = startPrize;
                randInt = getRandomIntInclusive(0, maxNum);
                continue;
            } else {
                break;
            }
        }
    }
} else {
    alert('You did not become a billionaire, but can.');
}