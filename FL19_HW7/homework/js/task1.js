// Your code goes here

function isNumber(data) {
    return typeof data === 'number' && !isNaN(data);
}

let a, b;
let resArr = [];

while (!isNumber(a) || !isNumber(b) || a >= b) {
    a = parseInt(prompt('First number:'), 10);
    b = parseInt(prompt('Second number:'), 10);
    if (!isNumber(a) || !isNumber(b) || a >= b) {
        alert('Invalid input data');
    } else {
        for (let i = a + 1; i < b; i++) {
            resArr.push(i);
        }
        console.log(resArr);
        alert(`Numbers between : ${resArr}`);
    }
}

console.log(isNumber('1'));