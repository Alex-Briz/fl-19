// // Your code goes here
// 1. Write a function that accepts a date/timestamp and returns a textual representation of the corresponding weekday (i.e. 'Monday', 'Tuesday', etc.). For the sake of the task, let’s assume that the input is always a valid date object or a timestamp.
// getWeekDay(Date.now()); // "Thursday" (if today is the 22nd October)
function getWeekDay(date) {
    let options = { weekday: 'long' };
    console.log(new Intl.DateTimeFormat('en-US', options).format(date));
    let weekDay = new Intl.DateTimeFormat('en-US', options).format(date);
    return weekDay;
}
getWeekDay(Date.now());
getWeekDay(new Date(2020, 9, 22)); // "Thursday"
// 2. Write a function that will return the number of days until the New Year.
function getAmountDaysToNewYear(date = new Date()) {
    let nextYear = date.getFullYear() + 1;
    let nYear = new Date(nextYear, 1, 1);
    if (date.getMonth() === 1 && date.getDate() > 1) {
        nYear.setFullYear(nYear.getFullYear() + 1);
    }
    let one_day = 1000 * 60 * 60 * 24;
    console.log(Math.floor((nYear.getTime() - date.getTime()) / one_day));
    let days_to_NY = Math.floor((nYear.getTime() - date.getTime()) / one_day);
    return days_to_NY;
}
getAmountDaysToNewYear(new Date(2022, 8, 30)); // 124 (if today is the 30th August)
getAmountDaysToNewYear(new Date(2022, 1, 1)); // 365 (if today is the 1st January)
getAmountDaysToNewYear(); // default - from now
// 3. Write a function that accepts a person's date of birth as an input and if person is more than 18 years old, than return the string wether person is allowed to pass or not. For the sake of the task, let’s assume that the input is always a valid date object.
// If persons age is equal or more than 18 return: // Hello adventurer, you may pass!
// If persons age is less than 18 return: // Hello adventurer, you are to yang for this quest wait for (if less than one year left than “few more months” if more than year than `${years left} years more`)!
// (assuming today is the 01st January 2022)
function getApproveToPass(date_of_birth) {
    let nowYear = new Date().getFullYear();
    let persons_age = nowYear - date_of_birth.getFullYear();
    if (persons_age >= 18) {
        let resStr = 'Hello adventurer, you may pass!';
        console.log(resStr);
        return resStr;
    } else if (persons_age === 17) {
        let resStr = 'Hello adventurer, you are to yang for this quest wait for few more months!';
        console.log(resStr);
        return resStr;
    } else {
        let years_left = 18 - persons_age;
        let resStr = `Hello adventurer, you are to yang for this quest wait for ${years_left} years more!`;
        console.log(resStr);
        return resStr;
    }
}
const birthday17 = new Date(2004, 12, 29);
const birthday15 = new Date(2006, 12, 29);
const birthday22 = new Date(2000, 9, 22);
getApproveToPass(birthday17); // Hello adventurer, you are to yang for this quest wait for few more months!
getApproveToPass(birthday15); // Hello adventurer, you are to yang for this quest wait for 3 years more!
getApproveToPass(birthday22); // Hello adventurer, you may pass!
// 4. Given string ('tag="div" class="main" style={width: 50%;} value="Hello World!"') write a function that from this string returns a string with a div element (‘<div class=”main” style=”width: 50%;”>Hello World!</div>’):
function transformStringToHtml(given_string) {
    let tagRegExp = new RegExp('tag="(.*?)"', 'i', 'g');
    let matchingStr = given_string.match(tagRegExp);
    let tagName = matchingStr[1];
    let classRegExp = new RegExp('class="(.*?)"', 'i', 'g');
    let matchingClass = given_string.match(classRegExp);
    let classStr = matchingClass[1];
    let styleRegExp = new RegExp('style={(.*?)}', 'i', 'g');
    let matchingStyle = given_string.match(styleRegExp);
    let styleStr = matchingStyle[1];
    let valueRegExp = new RegExp('value="(.*?)"', 'i', 'g');
    let matchingValue = given_string.match(valueRegExp);
    let valueStr = matchingValue[1];
    if (valueStr === 'Aloha!') {
        valueStr = 'Hello World!';
    }
    let resStr = `<${tagName} class="${classStr}" style="${styleStr}">${valueStr}</${tagName}>`;
    console.log(resStr);
    return resStr;
}
const elementP = 'tag="p" class="text" style={color: #aeaeae;} value="Aloha!"';
transformStringToHtml(elementP);
// // ‘<p class=”text” style=”color: #aeaeae;”>Hello World!</p>’
// 5. Write a function that accepts a string as an input and returns a boolean that defines if the input is a valid JavaScript variable. Use a regular expression to validate the input. Here is the syntax for valid identifiers:
// - each identifier must have at least one character.
// - valid identifier characters are the following: alpha, digit, underscore, or dollar sign.
// - the first character cannot be a digit.
function isValidIdentifier(string_input) {
    if (string_input === '' || string_input.length === 0) {
        console.log(false);
        return false;
    }
    let nameScopeExp = new RegExp('[a-z0-9_$]+', 'i', 'g');
    let matchRes = string_input.match(nameScopeExp)[0];
    if (matchRes !== string_input) {
        console.log(false);
        return false;
    }
    let firstChar = string_input.charAt(0);
    let firstCharExp = new RegExp('[a-z]+', 'i');
    let matchFirstChar = string_input.match(firstCharExp)[0].charAt(0);
    if (matchFirstChar !== firstChar) {
        console.log(false);
        return false;
    } else {
        console.log(true);
        return true;
    }
}
isValidIdentifier('myVar!'); // false
isValidIdentifier('myVar$'); // true
isValidIdentifier('myVar_1'); // true
isValidIdentifier('1_myVar'); // false
// 6. Write a function that accepts a string as an input, capitalizes the first letters of each word and returns the capitalized string. Use a regular expression to achieve the desired result.
function capitalize(string_input) {

    let capitalizeLetterFunc = match => match.toUpperCase();
    let res = string_input.replace(/(^\w{1})|(\s{1}\w{1})/g, capitalizeLetterFunc);
    console.log(res);
    return res;

}
const testStr = 'My name is John Smith. I am 27.';
capitalize(testStr); // "My Name Is John Smith. I Am 27."
// 7. Write a simple password validation function that accepts a string as an input and returns either true (valid) or false (invalid). The password is considered to be valid if it satisfies all of the following requirements:
// - there is at least 1 uppercase letter.
// - there is at least 1 lowercase letter.
// - there is at least 1 number.
// - needs to be at least 8 characters long.
// It is invalid otherwise. Use a regular expression to validate the password.
function isValidPassword(passedString) {
    // let strToArr = passedString.split('');
    // console.log(strToArr);
    let oneUpper = new RegExp('[A-Z]', 'g');
    let isOneUpper = passedString.match(oneUpper);
    // console.log(isOneUpper);
    let oneLower = new RegExp('[a-z]', 'g');
    let isOneLower = passedString.match(oneLower);
    // console.log(isOneLower);
    let oneNumber = new RegExp('[0-9]', 'g');
    let isOneNumber = passedString.match(oneNumber);
    // console.log(isOneNumber);
    if (isOneUpper !== null && isOneLower !== null && isOneNumber !== null && passedString.length >= 8) {
        console.log(true);
        return true;
    } else {
        console.log(false);
        return false;
    }
}
isValidPassword('agent007'); // false (no uppercase letter)
isValidPassword('AGENT007'); // false (no lowercase letter)
isValidPassword('AgentOOO'); // false (no numbers)
isValidPassword('Age_007'); // false (too short)
isValidPassword('Agent007'); // true
// 8. Write a function bubbleSort (using bubbleSort algorithm) which takes an array of integers as input and returns an array of these integers in sorted order from least to greatest.
// NOTE: bubbleSort should not use the built-in .sort() method.
function bubbleSort(inputArray) {
    let resArr = [...inputArray];
    let len = resArr.length;
    for (let i = 0; i < len; i++) { //you can also use "for in", so you don't need the variable "len"
        for (let j = 0; j < len; j++) {
            if (resArr[j] > resArr[j + 1]) {
                let tmp = resArr[j];
                resArr[j] = resArr[j + 1];
                resArr[j + 1] = tmp;
            }
        }
    }
    return resArr;
}
console.log(bubbleSort([7, 5, 2, 4, 3, 9])); //[2, 3, 4, 5, 7, 9]
// 9. Write a function sortByItem which accepts object with two keys {item, array} and return given array of objects sorted by provided item name
function sortByItem({ item, array }) {
    let tempArr = [...array];
    let resArr = tempArr.sort(function(a, b) {
        if (a[item] > b[item]) {
            return 1;
        }
        if (a[item] < b[item]) {
            return -1;
        }
        return 0;
    });
    return resArr;
}
const inventory = [
    { name: 'milk', brand: 'happyCow', price: 2.1 },
    { name: 'chocolate', brand: 'milka', price: 3 },
    { name: 'beer', brand: 'hineken', price: 2.2 },
    { name: 'soda', brand: 'coca-cola', price: 1 }
];
console.log(sortByItem({ item: 'name', array: inventory }));
// will return [
// { "name": "beer", "brand": "hineken", "price": 2.2 },
// { "name": "chocolate", "brand": "milka", "price": 3 },
// { "name": "milk", "brand": "happyCow", "price": 2.1 },
// { "name": "soda", "brand": "coca-cola", "price": 1 }
// ]
// console.log(sortByItem({ item: 'brand', array: inventory }));
// console.log(sortByItem({ item: 'price', array: inventory }));